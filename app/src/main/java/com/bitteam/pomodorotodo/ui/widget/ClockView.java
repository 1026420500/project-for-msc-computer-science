package com.bitteam.pomodorotodo.ui.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;

import androidx.annotation.Nullable;

import com.bitteam.pomodorotodo.R;

import lombok.Setter;

public class ClockView extends View {

    private int centerX;
    private int centerY;
    private int radius;
    private Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint timePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private int mColor = Color.parseColor("#D1D1D1");
    private RectF mRectF = new RectF();
    public static final float START_ANGLE = -90;
    public static final int MAX_TIME = 60;
    private float sweepVelocity = 0;
    private String textTime = "00:00";


    private int time;


    private static int tomato;


    private int countdownTime;
    private float touchX;
    private float touchY;
    private float offsetX;
    private float offsetY;
    private boolean isStarted;
    ValueAnimator valueAnimator;



    CountDownTimer timer;

    public ClockView(Context context) {
        super(context);
    }  //重写构造方法

    public ClockView(Context context, @Nullable AttributeSet attrs) {  //重写带接收xml属性信息的构造方法
        super(context, attrs);
    }

    public ClockView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) { //第三个参数是接收默认的属性赋值
        super(context, attrs, defStyleAttr);
    }

    public static float dpToPixel(float dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        return dp * metrics.density;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        centerX = width / 2;
        centerY = height / 2;
        radius = (int) dpToPixel(120);  // 3/4 inch
        setMeasuredDimension(width, height);

    }

    @Override
    protected void onDraw(Canvas canvas) { //
        super.onDraw(canvas);
        mRectF.set(centerX - radius, centerY - radius, centerX + radius, centerY + radius); //设置一个正方形，用于绘制灰弧

        canvas.save();
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(dpToPixel(5));
        canvas.drawCircle(centerX, centerY, radius, mPaint);
        canvas.restore();
        canvas.save();
        mPaint.setColor(mColor);
        canvas.drawArc(mRectF, START_ANGLE, 360 * sweepVelocity, false, mPaint);//useCenter:false表示不需要绘制弧与圆心的连线
        canvas.restore();

        canvas.save();
        timePaint.setColor(Color.BLACK);
        timePaint.setStyle(Paint.Style.FILL);
        timePaint.setTextSize(dpToPixel(40));
        canvas.drawText(textTime, centerX - timePaint.measureText(textTime) / 2,
            centerY - (timePaint.ascent() + timePaint.descent()) / 2, timePaint);
        canvas.restore();
    }



    private boolean isContained(float x, float y) {
        if (Math.sqrt((x - centerX) * (x - centerX) + (y - centerY) * (y - centerY)) > radius) {
            return false;
        } else {
            return true;
        }
    }

    private String formatTime(int time) {
        StringBuilder sb = new StringBuilder();
        if (time < 10) {
            sb.append("0" + time + ":00");
        } else {
            sb.append(time + ":00");
        }
        return sb.toString();
    }

    private String formatCountdownTime(int countdownTime) {
        StringBuilder sb = new StringBuilder();
        int minute = countdownTime / 60;
        int second = countdownTime - 60 * minute;
        if (minute < 10) {
            sb.append("0" + minute + ":");
        } else {
            sb.append(minute + ":");
        }
        if (second < 10) {
            sb.append("0" + second);
        } else {
            sb.append(second);
        }
        return sb.toString();
    }

    public void start() {

        if (time > 0 && countdownTime == 0) countdownTime = time * 60;
        invalidate();


        if (countdownTime == 0 || isStarted) {
            return;
        }


        isStarted = true;


        valueAnimator = ValueAnimator.ofFloat(0, 1.0f);
        valueAnimator.setDuration(countdownTime * 1000);
        valueAnimator.setInterpolator(new LinearInterpolator()); //在countdownTime * 1000ms的时间内，float值从0增加到1
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                sweepVelocity = (float) animation.getAnimatedValue();
                mColor = Color.parseColor("#D1D1D1");
                invalidate();
            }
        });
        valueAnimator.start();

        timer = new CountDownTimer(countdownTime * 1000 + 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                countdownTime = (countdownTime * 1000 - 1000) / 1000;
                time = countdownTime / 60;
                textTime = formatCountdownTime(countdownTime);
                invalidate();
                if (countdownTime <= 0 && startButton != null) startButton.callOnClick();
            }


            @Override
            public void onFinish() {
                mColor = Color.BLACK;
                sweepVelocity = 0;
                isStarted = false;
                invalidate();

            }
        }.start();
    }

    public void finish() {
        timer.onFinish();
        timer.cancel();
        valueAnimator.cancel();
        textTime = "00:00";
        invalidate();
    }
    
    public void stop() {
        timer.onFinish();
        timer.cancel();
        valueAnimator.cancel();
        invalidate();
    }

    public void setTime(int time) {
        this.time = time;
    }


    public void setCountdownTime(int countdownTime) {
        this.countdownTime = countdownTime;
    }
    
    public boolean canStart() {
        return time != 0;
    }

    public boolean isFinished() {
        return time <= 0 && countdownTime <= 0;
    }

    @Setter
    Button startButton = null;
}
