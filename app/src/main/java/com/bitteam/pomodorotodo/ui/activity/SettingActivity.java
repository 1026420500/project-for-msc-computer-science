package com.bitteam.pomodorotodo.ui.activity;


import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.bitteam.pomodorotodo.R;
import com.bitteam.pomodorotodo.mvp.model.DataBase.PomodoroTodoDB;
import com.bitteam.pomodorotodo.mvp.model.HistoryPomodoroListModel;

import lombok.Cleanup;

public class SettingActivity extends AppCompatActivity {

    private LinearLayout llClearData;
    private LinearLayout llQuestion;
    private LinearLayout llSelectDesk;
    HistoryPomodoroListModel hPModel = new HistoryPomodoroListModel(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setHomeButtonEnabled(true);
        mActionBar.setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_setting);
        initView();

        llClearData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder tipDialog = new AlertDialog.Builder(SettingActivity.this);
                tipDialog.setTitle("hint");
                tipDialog.setMessage("Are you sure you want to clear the data?");
                tipDialog.setNegativeButton("no", (dialog, which) -> dialog.cancel());
                tipDialog.setPositiveButton("yes", (dialog, which) -> {
                    hPModel.clearData();

                    dialog.cancel();
                });
                tipDialog.show();
            }
        });

        llQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder tipDialog = new AlertDialog.Builder(SettingActivity.this);
                tipDialog.setTitle("hint");
                tipDialog.setMessage("Should I ask when entering the plan?");
                tipDialog.setNegativeButton("no", (dialog, which) -> dialog.cancel());
                tipDialog.setPositiveButton("yes", (dialog, which) -> {

                    SharedPreferences sharedPreferences = getSharedPreferences("userDataCache", MODE_PRIVATE);
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    SharedPreferences.Editor editor = edit.putBoolean("question", true);
                    editor.commit();


                    dialog.cancel();
                });
                tipDialog.show();
            }
        });
        llSelectDesk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder tipDialog = new AlertDialog.Builder(SettingActivity.this);
                tipDialog.setTitle("hint");
                tipDialog.setMessage("Do you want to change the desktop style?");
                tipDialog.setNegativeButton("no,Use default", (dialog, which) -> {

                    SharedPreferences sharedPreferences = getSharedPreferences("userDataCache", MODE_PRIVATE);
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    SharedPreferences.Editor editor = edit.putBoolean("desktop", false);
                    editor.commit();


                    dialog.cancel();
                });
                tipDialog.setPositiveButton("yes,Use new", (dialog, which) -> {

                    SharedPreferences sharedPreferences = getSharedPreferences("userDataCache", MODE_PRIVATE);
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    SharedPreferences.Editor editor = edit.putBoolean("desktop", true);
                    editor.commit();


                    dialog.cancel();
                });
                tipDialog.show();
            }
        });
    }

    /**
     * Exit the current interface when you return
     */
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    private void initView() {
        llClearData = (LinearLayout) findViewById(R.id.ll_clear_data);
        llQuestion = (LinearLayout) findViewById(R.id.ll_question);
        llSelectDesk = (LinearLayout) findViewById(R.id.ll_select_desk);
    }

}
