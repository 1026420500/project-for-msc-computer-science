package com.bitteam.pomodorotodo.ui.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.bitteam.pomodorotodo.exception.DataException;
import com.bitteam.pomodorotodo.R;
import com.bitteam.pomodorotodo.mvp.model.DataBase.PomodoroTodoDB;
import com.bitteam.pomodorotodo.mvp.model.HistoryPomodoroListModel;
import com.bitteam.pomodorotodo.mvp.model.StandardPomodoroListModel;
import com.bitteam.pomodorotodo.mvp.model.bean.HistoryPomodoroBean;
import com.bitteam.pomodorotodo.mvp.model.bean.StandardPomodoroBean;
import com.bitteam.pomodorotodo.ui.widget.ClockView;
import com.bitteam.pomodorotodo.util.Sound;

import java.util.Date;

/**
 *The lock screen interface
 */
public class FocusActivity extends AppCompatActivity {

    StandardPomodoroListModel sPModel = new StandardPomodoroListModel(this, null);
    HistoryPomodoroListModel hPModel = new HistoryPomodoroListModel(this);
    private Sound mSound;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSound = new Sound(getApplicationContext());
        setContentView(R.layout.activity_focus);
        initData();
        initView();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {



            hPBean.setEndTime(new Date());
            hPBean.setTimeLength(Math.abs(hPBean.getEndTime().compareTo(hPBean.getStartTime())));
            hPModel.addNewHistoryPomodoro(hPBean);

            showDialog();
            return false;
        }else {
            return super.onKeyDown(keyCode, event);
        }
    }

    private void showDialog() {
        AlertDialog.Builder tipDialog = new AlertDialog.Builder(FocusActivity.this);
        tipDialog.setTitle("hint");
        tipDialog.setMessage("distant goal again, also can't afford to persistent insistence.");
        tipDialog.setPositiveButton("I'm going to quit", (dialog, which) -> FocusActivity.this.finish());
        tipDialog.setNegativeButton("Ok, hold on", (dialog, which) -> dialog.cancel());
        tipDialog.show();
    }

    @Override
    public boolean onSupportNavigateUp() {


        hPBean.setEndTime(new Date());
        hPBean.setTimeLength(Math.abs(hPBean.getEndTime().compareTo(hPBean.getStartTime())));
        hPModel.addNewHistoryPomodoro(hPBean);

        showDialog();
        return super.onSupportNavigateUp();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        mSound.release();
    }
    private void initView() {

        ImageView imageView = findViewById(R.id.clock_bg);


        SharedPreferences sharedPreferences = getSharedPreferences("userDataCache", MODE_PRIVATE);
        boolean desktop = sharedPreferences.getBoolean("desktop", false);

        if (desktop){
            imageView.setBackgroundResource(R.mipmap.bg2);
        } else {
            imageView.setBackgroundResource(R.drawable.login_bg);
        }




        findViewById(R.id.bt_music).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mSound.stop();

            }
        });

        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setHomeButtonEnabled(true);
        mActionBar.setDisplayHomeAsUpEnabled(true);

        clock = findViewById(R.id.clock);
        clock.setTime(sPBean.getTimeLength());

        Button startButton = findViewById(R.id.btn_start);
        clock.setStartButton(startButton);
        startButton.setOnClickListener(v -> {

            if (sPBean.getTimeLength() == 0) {

                startButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_play_arrow_white_24dp, 0, 0, 0);

                AlertDialog.Builder tipDialog = new AlertDialog.Builder(FocusActivity.this);
                tipDialog.setTitle("hint");
                tipDialog.setMessage("The time is over, please return.");
                tipDialog.setPositiveButton("yes", (dialog, which) -> dialog.cancel());
                tipDialog.show();
                return;
            }

            if (!clock.isFinished()) {
                if (started) {
                    mSound.pause();
                    clock.stop();
                    startButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_play_arrow_white_24dp, 0, 0, 0);
                    started = false;
                } else {


                    SharedPreferences sharedPreferences1 = getSharedPreferences("userDataCache", MODE_PRIVATE);
                    boolean question = sharedPreferences.getBoolean("question", false);

                    if (question){
                        AlertDialog.Builder tipDialog = new AlertDialog.Builder(FocusActivity.this);
                        tipDialog.setTitle("hint");
                        tipDialog.setMessage("Should I start poromoto?");
                        tipDialog.setNegativeButton("no", (dialog, which) -> dialog.cancel());
                        tipDialog.setPositiveButton("yes", (dialog, which) -> {
                            mSound.play();
                            clock.start();
                            startButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_pause_white_24dp, 0, 0, 0);
                            started = true;
                            dialog.cancel();
                        });
                        tipDialog.show();
                    } else {


                        mSound.play();
                        clock.start();
                        startButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_pause_white_24dp, 0, 0, 0);
                        started = true;
                    }



                }
            } else {
                clock.finish();
                startButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_play_arrow_white_24dp, 0, 0, 0);
                started = false;
                mSound.stop();
                AlertDialog.Builder tipDialog = new AlertDialog.Builder(FocusActivity.this);
                tipDialog.setTitle("hint");
                tipDialog.setMessage("The time is over, please return.");
                tipDialog.setPositiveButton("yes", (dialog, which) -> dialog.cancel());
                tipDialog.show();
            }
        });
    }

    private void initData() {

        started = false;

        pomodoroId = getIntent().getIntExtra(PomodoroTodoDB.HistoryPomodoroTable.ID, -1);
        if (pomodoroId == -1) throw new DataException("can't find data with _id");

        pomodoroType = getIntent().getStringExtra(PomodoroTodoDB.HistoryPomodoroTable.POMODORO_TYPE);
//        if (pomodoroType == null) throw new DataException("can't find data pomodoro type");

//        sPModel.changePomodoroType(sPModel.getPomodoroTypeById(pomodoroId));

        sPBean = sPModel.getPomodoroById(pomodoroId);

        hPBean = new HistoryPomodoroBean();

        hPBean.setName(sPBean.getName());
        hPBean.setTag(sPBean.getTag());
        hPBean.setPomodoro_type(pomodoroType);
        hPBean.setPomodoroId(sPBean.get_id());
        hPBean.setDescription(sPBean.getDescription());
        hPBean.setPictures(sPBean.getPictures());
        hPBean.setStrict(sPBean.isStrict());
        // 启动历史事件计时
        hPBean.setStartTime(new Date());
    }

    private HistoryPomodoroBean hPBean = null;

    private StandardPomodoroBean sPBean = null;

    private int pomodoroId = -1;
    private String pomodoroType = null;

    private ClockView clock;
    private boolean started = false;
}
