package com.bitteam.pomodorotodo.mvp.contract;

import com.bitteam.pomodorotodo.mvp.base.BasePresenter;
import com.bitteam.pomodorotodo.mvp.base.BaseView;

/**
 * Home Page Protocol (Example)
 * Define one for each functional direction or page
 * Used to negotiate display and data interactions
 */
public interface HomeContract {

    interface IView extends BaseView {}

    interface IPresenter extends BasePresenter {}

}
