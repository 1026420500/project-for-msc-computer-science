package com.bitteam.pomodorotodo.exception;

/**
 * An exception in the UI section
 */
public class UIException extends RuntimeException {

    public UIException(String message) {
        super(message);
    }
}
