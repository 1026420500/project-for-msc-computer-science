package com.bitteam.pomodorotodo.exception;

/**
 * Parameters of the abnormal
 */
public class ArgsException extends RuntimeException {

    public ArgsException(String message) {
        super(message);
    }
}
